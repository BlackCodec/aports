# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-mimesis
pkgver=12.1.1
pkgrel=0
pkgdesc="The Fake Data Generator"
url="https://mimesis.name"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-gpep517 py3-poetry-core"
checkdepends="py3-colorama py3-pytest-xdist py3-validators py3-tz"
subpackages="$pkgname-pyc"
source="https://github.com/lk-geimfari/mimesis/archive/v$pkgver/mimesis-$pkgver.tar.gz"
builddir="$srcdir/mimesis-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -k 'not test_schema and not test_internet' --ignore=tests/test_builtins/test_usa_spec.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
f68612195eff169e5a617cb152e366bfb04d0ceb818ae11d588e045f5df154a41bfda5336d30d1fd7fb0d374498eb2eb889be2528ddc2ae36b7c50d4c5097ecd  mimesis-12.1.1.tar.gz
"
