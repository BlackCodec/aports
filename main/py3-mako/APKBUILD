# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-mako
_pkgname=Mako
pkgver=1.3.0
pkgrel=1
pkgdesc="Python3 fast templating language"
url="https://www.makotemplates.org/"
arch="noarch"
license="MIT"
depends="python3 py3-markupsafe"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-babel py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver

replaces="py-mako" # Backwards compatibility
provides="py-mako=$pkgver-r$pkgrel" # Backwards compatibility

# secfixes:
#   1.2.2-r0:
#     - CVE-2022-40023

build() {
	gpep517 build-wheel --wheel-dir .dist --output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# unpackaged py3-lingua
	.testenv/bin/python3 -m pytest --ignore test/ext/test_linguaplugin.py
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
1be454ceff8942ee50052b3b2dec392f236aad2111828a6962ca80bc0d9692f632b7d992b161fb206c10db19e94f96a0688633d03c7b54e01b4f843283b80cb4  Mako-1.3.0.tar.gz
"
